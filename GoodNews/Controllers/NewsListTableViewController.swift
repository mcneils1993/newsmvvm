
import UIKit

class NewsListTableViewController: UITableViewController {
	
	private var articleListViewModel: ArticleListViewModel?
	var counter = 0
	
	override func viewDidLoad() {
		super.viewDidLoad()
		setup()
		
		let action = #selector(refreshDidStart(sender:event:))
		refreshControl = UIRefreshControl()
		refreshControl?.tintColor = .white
		refreshControl?.addTarget(self, action: action, for: .valueChanged)
		
	}
	
	@objc func refreshDidStart(sender: UIRefreshControl?, event: UIEvent?) {
		self.tableView.beginRefreshing()
		let url: URL!
		if counter.isMultiple(of: 2) {
			url = URL(string: "https://newsapi.org/v2/top-headlines?country=ca&apiKey=0e589e1500fc43e8b707fe4ad3657b92")
		} else {
			url = URL(string: "https://newsapi.org/v2/top-headlines?country=us&apiKey=0e589e1500fc43e8b707fe4ad3657b92")
		}
		counter += 1
		WebService().getArticles(url: url) { [weak self] articles in
			guard let self = self else { return }
			if let results = articles {
				self.articleListViewModel?.articles.removeAll()
				self.articleListViewModel = ArticleListViewModel(totalResults: results.articles.count, articles: results.articles)
				DispatchQueue.main.async {
					self.tableView.reloadData()
				}
			}
			DispatchQueue.main.async {
				self.tableView.endRefreshing()
			}
		}
		
//		DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//			self.tableView.endRefreshing()
//		}
	}
	
	func setup() {
		tableView.estimatedRowHeight = 80
		tableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
		tableView.register(ArticleTableViewCell.self, forCellReuseIdentifier: "cell")
		
		guard let url = URL(string: "https://newsapi.org/v2/top-headlines?country=ca&apiKey=0e589e1500fc43e8b707fe4ad3657b92") else { return }
		
		WebService().getArticles(url: url) { [weak self] articles in
			guard let self = self else { return }
			if let results = articles {
				self.articleListViewModel = ArticleListViewModel(totalResults: results.articles.count, articles: results.articles)
				DispatchQueue.main.async {
					self.tableView.reloadData()
				}
			}
		}
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return articleListViewModel?.totalResults ?? 0
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? ArticleTableViewCell, let articleVM = articleListViewModel else {
			return UITableViewCell()
		}
		let articleViewModel = articleVM.articleAtIndex(indexPath.row)
		cell.configure(cell: articleViewModel)
		
		return cell
	}
}
